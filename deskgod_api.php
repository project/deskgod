<?php
  //set the working directory to your Drupal root
  //chdir('/home/public_html/drupal/');
  chdir('../../../../');

  //require the bootstrap include
  require_once './includes/bootstrap.inc';

  //Load Drupal

  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

  //pwd=XXXX&email=$email&customer_id=$customer_id&customer_name=$customer_name&tag=[chat][ticket][ticketzoom]
  $_GET['uid'] = $_GET['customer_id'];
  $_GET['mail'] = $_GET['email'];
  
  $user_search_params = false;
  if(isset($_GET['uid'])){
  	$user_search_params = array('uid' => intval($_GET['uid']));
  } elseif(isset($_GET['mail'])){
  	$user_search_params = array('mail' => trim($_GET['mail']));
  }
  
  $no_record = 'No record found.';
  
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr"> 
  <head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="text/css" rel="stylesheet" media="all" href="../../../../modules/system/system.css" /> 
  </head>
  <body>
<?php
  if($user_search_params){
    $someuser = user_load($user_search_params);
?>
<table class="body-table" style="color:#494949; font-family:Verdana, sans-serif; font-size:10px;">
  <tbody>
  <?php if($someuser->uid != ''){?>
    <tr class="odd">
      <th align="right" valign="top"> User Id: </th>
      <td align="left" valign="top"><?php echo $someuser->uid; ?></td>
    </tr>
    <tr class="even">
      <th align="right" valign="top"> Name: </th>
      <td align="left" valign="top"><?php echo $someuser->name; ?></td>
    </tr>
    <tr class="odd">
      <th align="right" valign="top"> E-mail: </th>
      <td align="left" valign="top"><?php echo $someuser->mail; ?></td>
    </tr>
    <tr class="even">
      <th align="right" valign="top"> Account created: </th>
      <td align="left" valign="top"><?php echo date('m.d.Y' , $someuser->created); ?></td>
    </tr>
    <tr class="odd">  
      <th align="right" valign="top"> Last logged in: </th>
      <td align="left" valign="top"><?php echo date('m.d.Y', $someuser->access); ?></td>
    </tr>
  <?php } else {
    echo "<tr><th>$no_record</th></tr>\n";
        } ?>
  </tbody>
</table>
<?php
  } else {
  	echo "<p>$no_record</p>";
  }
?>
  </body>
</html>