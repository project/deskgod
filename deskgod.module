<?php

/*
 * @file
 * Drupal Module: deskgod
 * Adds the required Javascript to the bottom of all your Drupal pages
 * to allow tracking by the DeskGod statistics package.
 *
 * @author: Yanko Kirev <www.deskgod.com>
 */

function deskgod_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/deskgod':
      return t('<a href="@dg_url">DeskGod</a> is a free statistics package based on the DeskGod system. This module provides services to better integrate Drupal with DeskGod.', array('@dg_url' => 'http://www.deskgod.com/'));
  }
}

function deskgod_menu() {
  $items['admin/settings/deskgod'] = array(
    'title' => 'DeskGod',
    'description' => 'Configure the settings used to generate your DeskGod tracking code.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deskgod_admin_settings_form'),
    'access arguments' => array('administer DeskGod'),
    'file' => 'deskgod.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

function deskgod_init() {
  global $user;
  $id = variable_get('deskgod_sitecode', '');

  if (!empty($id) && _deskgod_visibility_pages()){
    $scope = variable_get('deskgod_js_scope', 'footer');

    // Should a local cached copy of js_deskgodit.js be used?
    $js_file = 'js_deskgodit.js';
    $url = 'http://www.deskgod.com/all/members/roi_watcher/'. $js_file;

    if (variable_get('deskgod_cache', 0) && (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PUBLIC) && $source = _deskgod_cache($url)) {
      drupal_add_js($source, 'module', $scope);
    }
    else {
      $script = 'var dgJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");';
      $script .= 'document.write(unescape("%3Cscript src=\'" + dgJsHost + "www.deskgod.com/all/roi_watcher/'. $js_file .'\' type=\'text/javascript\'%3E%3C/script%3E"));';
      drupal_add_js($script, 'inline', $scope);
    }
    
  }
}

/**
 * Implementation of hook_footer() to insert Javascript at the end of the page.
 */
function deskgod_footer($main = 0) {
  global $user;

  $id = variable_get('deskgod_sitecode', '');

  if (!empty($id) && _deskgod_visibility_pages()){

    // Add User profile segmentation values.
    if (is_array($profile_fields = variable_get('deskgod_segmentation', '')) && ($user->uid > 0)) {

      $p = module_invoke('profile', 'load_profile', $user);

      $fields = array();
      foreach ($profile_fields as $field => $title) {
        $value = $user->$field;

        if (is_array($value)) {
          $value = implode(',', $value);
        }

        $fields[$field] = $value;
      }

      // Only show segmentation variable if there are specified fields.
      $segmentation = '';
      if (count($fields) > 0) {
        $segmentation = 'pageTracker._setVar('. drupal_to_js(implode(':', $fields)) .');';
      }
    }

    // Site search tracking support.
    $url_custom = '';
    if (module_exists('search') && variable_get('deskgod_site_search', FALSE) && arg(0) == 'search' && $keys = search_get_keys()) {
      $url_custom = drupal_to_js(url('search/'. arg(1), array('query' => 'search='. $keys)));
    }

    // If this node is a translation of another node, pass the original
    // node instead.
    if (module_exists('translation') && variable_get('deskgod_translation_set', 0)) {
      // Check we have a node object, it supports translation, and its
      // translated node ID (tnid) doesn't match its own node ID.
      $node = menu_get_object();
      if ($node && translation_supported_type($node->type) && isset($node->tnid) && ($node->tnid != $node->nid)) {
        $source_node = node_load($node->tnid);
        $languages = language_list();
        $url_custom = drupal_to_js(url('node/'. $source_node->nid, array('language' => $languages[$source_node->language])));
      }
    }

    // Build tracker code for footer.
    $script = 'try{';
    $script .= "var dg_userid='".$user->uid."'; var dg_useremail='".$user->mail."'; var dg_username='".$user->name."';";
    $script .= 'deskgod_it2('. drupal_to_js($id) .',dg_userid,dg_useremail,dg_username);';
    if (!empty($segmentation)) {
      $script .= $segmentation;
    }
    $script .= "document.write('<div id=\"fclLayer\"></div><div id=\"dgChatLayer\" style=\"position:fixed; visibility:visible; right:0px; bottom:0px; z-index:9999;\"></div>')";
    $script .= '} catch(err) {}';

    drupal_add_js($script, 'inline', 'footer');
  }
}

/**
 * Implementation of hook_requirements().
 */
function deskgod_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    // Raise warning if DeskGod user account has not been set yet.
    if (!preg_match('/^DG-\w{6,}[+,-]\d{1,2}:\d{2}+$/', variable_get('deskgod_sitecode', 'DG-'))) {
      $requirements['deskgod'] = array(
        'title' => t('DeskGod module'),
        'description' => t('DeskGod module has not been configured yet. Please configure its settings from the <a href="@url">DeskGod settings page</a>.', array('@url' => url('admin/settings/deskgod'))),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Not configured'),
      );
    }
  }

  return $requirements;
}

/**
 * Implementation of hook_cron().
 */
function deskgod_cron() {
  // Regenerate the DeskGod ga.js every day.
  if (time() - variable_get('deskgod_last_cache', 0) >= 86400) {
    // New DeskGod version.
    file_delete(file_directory_path() .'/deskgod/ga.js');

    // Clear aggregated JS files.
    if (variable_get('preprocess_js', 0)) {
      drupal_clear_js_cache();
    }

    variable_set('deskgod_last_cache', time());
  }
}

/**
 * Download and cache the js_deskgodit.js file locally.
 * @param $location
 *   The full URL to the external javascript file.
 * @return mixed
 *   The path to the local javascript file on success, boolean FALSE on failure.
 */
function _deskgod_cache($location) {
  $directory = file_directory_path() .'/deskgod';
  $file_destination = $directory .'/'. basename($location);
  if (!file_exists($file_destination)) {
    $result = drupal_http_request($location);
    if ($result->code == 200) {
      // Check that the files directory is writable
      if (file_check_directory($directory, FILE_CREATE_DIRECTORY)) {
        return file_save_data($result->data, $directory .'/'. basename($location), FILE_EXISTS_REPLACE);
      }
    }
  }
  else {
    return $file_destination;
  }
}

/**
 * Based on visibility setting this function returns TRUE if DG code should
 * be added to the current page and otherwise FALSE.
 */
function _deskgod_visibility_pages() {
  static $page_match;

  // Cache visibility setting in hook_init for hook_footer.
  if (!isset($page_match)) {

    $visibility = variable_get('deskgod_visibility', 0);
    $pages = variable_get('deskgod_pages', '');

    // Match path if necessary.
    if (!empty($pages)) {
      if ($visibility < 2) {
        $path = drupal_get_path_alias($_GET['q']);
        // Compare with the internal and path alias (if any).
        $page_match = drupal_match_path($path, $pages);
        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }
        // When $visibility has a value of 0, the block is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      else {
        $page_match = drupal_eval($pages);
      }
    }
    else {
      $page_match = TRUE;
    }

  }
  return $page_match;
}
