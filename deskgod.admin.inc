<?php

/**
 * @file
 * Administrative page callbacks for the deskgod module.
 */

/**
 * Implementation of hook_admin_settings() for configuring the module
 */
function deskgod_admin_settings_form(&$form_state) {

	$form['sitecode'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );

  $current_domain = $_SERVER['HTTP_HOST'];
  $dgapi = explode('?', $_SERVER['REQUEST_URI']);
  $dgapi = 'http://' . $current_domain . $dgapi[0] . 'sites/all/modules/deskgod/deskgod_api.php';
  
  $form['sitecode']['deskgod_sitecode'] = array(
    '#type' => 'textfield',
    '#title' => t('Website tracking code with DeskGod '),
    '#default_value' => variable_get('deskgod_sitecode', 'DG-'),
    '#size' => 18,
    '#maxlength' => 25,
    '#required' => TRUE,
    '#description' => t('The site tracking code is unique to the websites domain. Click the <strong>Websites</strong> link in your DeskGod account on the <strong>Options</strong> page to find the tracking code (DG-xxxxxx-xx:xx) of your site. You can obtain a tracking code from the <a href="@url">DeskGod</a> website.<br/>To enable the user lookup API function for DeskGod, open the advanced options of this site in your DeskGod account and copy-paste the following address in the "API-Box" field:<br/>@dgapi', array('@url' => 'http://www.deskgod.com/', '@dgapi' => $dgapi)),
  );

	$form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific tracking settings'),
    '#collapsible' => FALSE
  );

  $access = user_access('use PHP for tracking visibility');
  $visibility = variable_get('deskgod_visibility', 0);
  $pages = variable_get('deskgod_pages', '');

  if ($visibility == 2 && !$access) {
    $form['page_vis_settings'] = array();
    $form['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
  }
  else {
    $options = array(t('Add to every page except the listed pages.'), t('Add to the listed pages only.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['page_vis_settings']['deskgod_visibility'] = array(
      '#type' => 'radios',
      '#title' => t('Add tracking to specific pages'),
      '#options' => $options,
      '#default_value' => $visibility,
    );
    $form['page_vis_settings']['deskgod_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $pages,
      '#description' => $description,
      '#wysiwyg' => FALSE,
    );
  }

  // Advanced feature configurations.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['deskgod_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cache tracking code file locally'),
    '#description' => t("If checked, the tracking code file is retrieved from DeskGod and cached locally. It is updated daily from DeskGod's servers to ensure updates to tracking code are reflected in the local copy. Do not activate this until after DeskGod has confirmed your tracker!"),
    '#default_value' => variable_get('deskgod_cache', 0),
  );
  if (variable_get('file_downloads', FILE_DOWNLOADS_PUBLIC) == FILE_DOWNLOADS_PRIVATE) {
    $form['advanced']['deskgod_cache']['#disabled'] = TRUE;
    $form['advanced']['deskgod_cache']['#description'] .= ' '. t('<a href="@url">Public file transfers</a> must be enabled to allow local caching.', array('@url' => url('admin/settings/file-system', array('query' => drupal_get_destination()))));
  }

  $form['advanced']['deskgod_js_scope'] = array(
    '#type' => 'select',
    '#title' => t('JavaScript scope'),
    '#description' => t("<strong>Warning:</strong> DeskGod recommends adding the external JavaScript files to footer for performance reasons."),
    '#options' => array(
      'footer' => t('Footer'),
      'header' => t('Header'),
    ),
    '#default_value' => variable_get('deskgod_js_scope', 'footer'),
  );

  return system_settings_form($form);
}

function deskgod_admin_settings_form_validate($form, &$form_state) {
  
	if (!preg_match('/^DG-\w{6,}[+,-]\d{1,2}:\d{2}+$/', $form_state['values']['deskgod_sitecode'])) {
    form_set_error('deskgod_sitecode', t('A valid DeskGod website tracking code is case sensitive and formatted like DG-xxxxxx-xx:xx.'));
  }

  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['deskgod_codesnippet_before'])) {
    form_set_error('deskgod_codesnippet_before', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }
  if (preg_match('/(.*)<\/?script(.*)>(.*)/i', $form_state['values']['deskgod_codesnippet_after'])) {
    form_set_error('deskgod_codesnippet_after', t('Do not include the &lt;script&gt; tags in the javascript code snippets.'));
  }

  // Trim some text area values.
  $form_state['values']['deskgod_pages'] = trim($form_state['values']['deskgod_pages']);
  $form_state['values']['deskgod_codesnippet_before'] = trim($form_state['values']['deskgod_codesnippet_before']);
  $form_state['values']['deskgod_codesnippet_after'] = trim($form_state['values']['deskgod_codesnippet_after']);
}
