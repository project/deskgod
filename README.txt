
Module: DeskGod
Author: Yanko Kirev <www.deskgod.com>


Description
===========
Adds the DeskGod tracking system to your website.

Requirements
============

* DeskGod account


Installation
============
* Copy the 'deskgod' module directory in to your Drupal
sites/all/modules directory as usual.


Usage
=====
In the settings page enter your DeskGod website tracking code.

All pages will now have the required JavaScript added to the
HTML footer can confirm this by viewing the page source from
your browser.

New approach to page tracking in 5.x-1.5 and 6.x-1.1
====================================================
With 5.x-1.5 and 6.x-1.1 there are new settings on the settings page at
admin/settings/googleanalytics. The "Page specific tracking" area now
comes with an interface that copies Drupal's block visibility settings.

The default is set to "Add to every page except the listed pages". By
default the following pages are listed for exclusion:

admin
admin/*
user/*/*
node/add*
node/*/*

These defaults are changeable by the website administrator or any other
user with 'administer deskgod' permission.

Like the blocks visibility settings in Drupal core, there is now a
choice for "Add if the following PHP code returns TRUE." Sample PHP snippets
that can be used in this textarea can be found on the handbook page
"Overview-approach to block visibility" at http://drupal.org/node/64135.

Advanced Settings
=================

To speed up page loading you may also cache the deskgod js
file locally. You need to make sure the site file system is in public
download mode.